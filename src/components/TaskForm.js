import React, { useState } from "react";
import { Input, Button } from "antd";

const TaskForm = ({ onSubmit, onCancel, initialValue = "" }) => {
  const [value, setValue] = useState(initialValue);

  return (
    <div style={{ display: "flex" }}>
      <Input
        placeholder="Type Any Task..."
        value={value}
        onChange={(e) => setValue(e.target.value)}
        id="task-input-field"
      />
      <Button id="cancel-btn" onClick={onCancel}>
        Cancel
      </Button>
      <Button id="done-btn" type="primary" onClick={() => onSubmit(value)}>
        Done
      </Button>
    </div>
  );
};

export default TaskForm;
