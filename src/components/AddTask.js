import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { addTask } from "state/tasksSlice";
import { PlusOutlined } from "@ant-design/icons";
import { Button } from "antd";
import TaskModal from "components/TaskModal";
import TaskForm from "components/TaskForm";

const AddTask = () => {
  const [addFormShow, setAddFormShow] = useState(false);
  const dispatch = useDispatch();

  const handleSubmit = (task) => {
    dispatch(
      addTask({
        task,
        date: "January 24th,2021 04:25 PM",
        isFinished: false,
      })
    );
    setAddFormShow(false);
  };

  return (
    <>
      <div
        style={{
          display: "flex",
          justifyContent: "center",
          marginTop: "40px",
        }}
      >
        <Button
          onClick={() => setAddFormShow(true)}
          size="large"
          type="circle"
          style={{
            backgroundColor: "rgb(29, 233, 14)",
            color: "white",
          }}
          icon={<PlusOutlined />}
        />
      </div>
      <TaskModal showModal={addFormShow} setShowModal={setAddFormShow}>
        <TaskForm
          onSubmit={handleSubmit}
          onCancel={() => setAddFormShow(false)}
        />
      </TaskModal>
    </>
  );
};

export default AddTask;
