import React from "react";
import TasksList from "components/TasksList";
import SelectedTask from "components/SelectedTask";
import AddTask from "components/AddTask";
import "./index.css";

const Pannel = () => {
  return (
    <div className="pannel-container">
      <TasksList />
      <SelectedTask />
      <AddTask />
    </div>
  );
};

export default Pannel;
