import React from "react";
import { Modal } from "antd";

const TaskModal = ({ showModal, setShowModal, children, ...props }) => {
  const handleCancel = () => {
    setShowModal(false);
  };

  return (
    <Modal
      centered
      visible={showModal}
      onCancel={handleCancel}
      footer={null}
      closable={false}
      destroyOnClose
      width={900}
      {...props}
    >
      {children}
    </Modal>
  );
};

export default TaskModal;
