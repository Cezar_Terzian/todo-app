import React from "react";
import { useSelector } from "react-redux";
import TaskItem from "components/TaskItem";
import { List } from "antd";

const TasksList = () => {
  const tasks = useSelector((state) => state.tasks.data);

  return (
    <List
      dataSource={tasks}
      renderItem={(task) => <TaskItem key={task.id} task={task} />}
    />
  );
};

export default TasksList;
