import React from "react";
import { useDispatch } from "react-redux";
import { deleteTask, setActiveTask } from "state/tasksSlice";
import { Button, Row, Col } from "antd";
import { DeleteFilled } from "@ant-design/icons";

const textStyle = {
  color: "#fff",
  margin: 0,
  padding: 0,
};

const TaskItem = ({ task: taskObject }) => {
  const dispatch = useDispatch();
  const { id, task, date, isFinished } = taskObject;

  const classNames =
    "task " + (isFinished ? "task-finished " : "task-not-finished ");

  return (
    <div onClick={() => dispatch(setActiveTask(id))} className={classNames}>
      <Row align="middle" justify="center">
        <Col span={22}>
          <p style={textStyle}>{date}</p>
          <h4 style={textStyle}>{task}</h4>
        </Col>
        <Col>
          <Button
            shape="circle"
            icon={<DeleteFilled />}
            onClick={() => dispatch(deleteTask(id))}
          />
        </Col>
      </Row>
    </div>
  );
};

export default TaskItem;
