import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { setIsFinished, deleteTask, editTask } from "state/tasksSlice";
import TaskModal from "components/TaskModal";
import TaskForm from "components/TaskForm";
import { Button, Row, Col } from "antd";
import {
  DeleteFilled,
  EditFilled,
  CloseOutlined,
  CheckOutlined,
} from "@ant-design/icons";

const SelectedTask = () => {
  const dispatch = useDispatch();
  const [editFormShow, setEditFormShow] = useState(false);
  const { activeTask } = useSelector((state) => state.tasks);
  const taskObject = useSelector((state) =>
    state.tasks.data.find((task) => task.id === activeTask)
  );

  if (!taskObject) return null;

  const { id, task, date } = taskObject;

  const handleEdit = (value) => {
    dispatch(
      editTask({
        id,
        value,
      })
    );
    setEditFormShow(false);
  };

  return (
    <>
      <div className="selected-task">
        <Row align="middle" justify="center">
          <Col span={18}>
            <p style={{ margin: 0, padding: 0 }}>{date}</p>
            <h4 style={{ margin: 0, padding: 0 }}>{task}</h4>
          </Col>
          <Col>
            <CustomButton
              onClick={() => dispatch(deleteTask(id))}
              style={{ backgroundColor: "rgb(235, 55, 56)" }}
              icon={<DeleteFilled />}
            />
            <CustomButton
              onClick={() => setEditFormShow(true)}
              type="primary"
              icon={<EditFilled />}
            />
            <CustomButton
              onClick={() => dispatch(setIsFinished({ id, isFinished: false }))}
              style={{ backgroundColor: "rgb(235, 55, 56)" }}
              icon={<CloseOutlined />}
            />
            <CustomButton
              onClick={() => dispatch(setIsFinished({ id, isFinished: true }))}
              style={{ backgroundColor: "rgb(29, 233, 14)" }}
              icon={<CheckOutlined />}
            />
          </Col>
        </Row>
      </div>
      <TaskModal showModal={editFormShow} setShowModal={setEditFormShow}>
        <TaskForm
          onSubmit={handleEdit}
          onCancel={() => setEditFormShow(false)}
          initialValue={task}
        />
      </TaskModal>
    </>
  );
};

export default SelectedTask;

function CustomButton({ icon, style, ...props }) {
  return (
    <Button
      {...props}
      style={{ marginLeft: "5px", color: "#fff", ...style }}
      shape="circle"
      icon={icon}
    />
  );
}
