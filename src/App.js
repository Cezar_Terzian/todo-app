import Pannel from "components/Pannel";
import "./App.css";

function App() {
  return (
    <div className="App">
      <h1>Todo List</h1>
      <Pannel />
    </div>
  );
}

export default App;
