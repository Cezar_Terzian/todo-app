import { createSlice } from "@reduxjs/toolkit";

let ID = 3;

const initialState = {
  data: [
    {
      id: 1,
      task: "Fix login & register form design",
      date: "January 24th,2021 04:25 PM",
      isFinished: true,
    },
    {
      id: 2,
      task: "Hello There...",
      date: "January 24th,2021 04:25 PM",
      isFinished: false,
    },
  ],
  activeTask: 1,
};

export const tasksSlice = createSlice({
  name: "tasks",
  initialState,
  reducers: {
    addTask: (state, action) => {
      state.data.push({
        id: ID,
        ...action.payload,
      });
      ID++;
    },
    deleteTask: (state, action) => {
      state.data.splice(
        state.data.findIndex((obj) => obj.id === action.payload),
        1
      );
    },
    setIsFinished: (state, action) => {
      state.data = state.data.map((task) =>
        task.id === action.payload.id
          ? { ...task, isFinished: action.payload.isFinished }
          : task
      );
    },
    setActiveTask: (state, action) => {
      state.activeTask = action.payload;
    },
    editTask: (state, action) => {
      state.data = state.data.map((task) =>
        task.id === action.payload.id
          ? { ...task, task: action.payload.value }
          : task
      );
    },
  },
});

// Action creators are generated for each case reducer function
export const { addTask, deleteTask, setIsFinished, setActiveTask, editTask } =
  tasksSlice.actions;

export default tasksSlice.reducer;
